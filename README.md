# jvvm and jvxh

This repository contains the tools I use to run Linux Virtual Machines on macOS.

- `jvvm`: a shim around Virtualization.framework that boots a VM. Inspired by [evansm7/vftool](https://github.com/evansm7/vftool).
- `jvxh`: a virtual machine manager that can boot a `jvvm` process. Originally a wrapper around Xhyve, hence the name.

Install both executables into `~/bin` using `make install`. Or somewhere else, as long as it's in `$PATH`.

Create a new VM with `jvxh init`. For Linux installation hints see `jvxh notes | less`.

## What this does

- Boot virtual machines in your macOS Terminal.
- A jvxh VM is a directory with some config files, a disk image and a Linux kernel.
- Your terminal becomes a serial console for the VM.
- Your VM gets a static random MAC address, based on the `uuid` file. On my machine, macOS assigns a 192.168.64.x IP address to the VM.
- Network connections from host to guest work.
- The guest can automatically reach the internet via the network of the host.
- Network connections from guest (192.168.64.x) to host (192.168.64.1) get blocked by a firewall somewhere.
- The best way to interact with the VM is to log in with SSH. To get your SSH key on it, use the serial console.
- When you run `poweroff` in the VM, it halts and you get your terminal back.
- The VM is assigned memory and CPU cores via the `config` file in the VM directory.
- Upgrading the Linux kernel on your VM is a chore but it can be done. You need to copy the new kernel and the initial ramdisk image from the VM to the host and put them in the `boot` subdirectory.
- The disk image is just a file. If it's not running you can snapshot and restore the VM with `jvxh snapshot` and `jvxh restore`.

# Expectations

I made these tools for myself. They work well enough so I do not
intend to add new features. If you are looking for a proper virtual
machine manager on macOS consider solutions like VirtualBox, Qemu,
VMware, Parallels etc.

Also consider [gyf304/vmcli](https://github.com/gyf304/vmcli) as an
alternative with a very similar design but more features.
